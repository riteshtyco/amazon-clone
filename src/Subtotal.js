import React from "react";
import "./Subtotal.css";

function Subtotal() {
  return (
    <div className="subtotal">
      renderText=
      {(value) => (
        <>
          <p>
            Subtotal (0 items): <strong>0</strong>
          </p>
          <small className="subtotal__gift">
            <input type="checkbox" /> This order contains a gift
          </small>
        </>
      )}
      decimalScal={2}
      value={0}
      displayType={"text"}
      thousandSeparator={true}
      prefix={"$"}
      <button>Proceed to checkout</button>
    </div>
  );
}

export default Subtotal;
