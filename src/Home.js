import React from "react";
import "./Home.css";
import Product from "./Product";

function Home() {
  return (
    <div className="home">
      <div className="home__container">
        <img
          className="home__image"
          src="https://images-eu.ssl-images-amazon.com/images/G/02/digital/video/merch2016/Hero/Covid19/Generic/GWBleedingHero_ENG_COVIDUPDATE__XSite_1500x600_PV_en-GB._CB428684220_.jpg"
          alt=""
        />
        <div className="home__row">
          <Product
            id="1"
            title="Beyonce IVY PARK"
            price={50}
            image="https://static.highsnobiety.com/thumbor/fTKYQ8ux97ZCfMIZj965knFXAXw=/960x1200/static.highsnobiety.com/wp-content/uploads/2019/12/09155239/beyonce-adidas-first-look-01.jpg"
            rating={5}
          />
          <Product
            id="2"
            title="Beyonce IVY PARK"
            price={50}
            image="https://static.highsnobiety.com/thumbor/fTKYQ8ux97ZCfMIZj965knFXAXw=/960x1200/static.highsnobiety.com/wp-content/uploads/2019/12/09155239/beyonce-adidas-first-look-01.jpg"
            rating={5}
          />
        </div>
        <div className="home__row">
          <Product
            id="3"
            title="Beyonce IVY PARK"
            price={50}
            image="https://static.highsnobiety.com/thumbor/fTKYQ8ux97ZCfMIZj965knFXAXw=/960x1200/static.highsnobiety.com/wp-content/uploads/2019/12/09155239/beyonce-adidas-first-look-01.jpg"
            rating={5}
          />
          <Product
            id="4"
            title="Beyonce IVY PARK"
            price={50}
            image="https://static.highsnobiety.com/thumbor/fTKYQ8ux97ZCfMIZj965knFXAXw=/960x1200/static.highsnobiety.com/wp-content/uploads/2019/12/09155239/beyonce-adidas-first-look-01.jpg"
            rating={5}
          />
          <Product
            id="5"
            title="Beyonce IVY PARK"
            price={50}
            image="https://static.highsnobiety.com/thumbor/fTKYQ8ux97ZCfMIZj965knFXAXw=/960x1200/static.highsnobiety.com/wp-content/uploads/2019/12/09155239/beyonce-adidas-first-look-01.jpg"
            rating={5}
          />
        </div>
        <div className="home__row">
          <Product
            id="6"
            title="Beyonce IVY PARK"
            price={50}
            image="https://static.highsnobiety.com/thumbor/fTKYQ8ux97ZCfMIZj965knFXAXw=/960x1200/static.highsnobiety.com/wp-content/uploads/2019/12/09155239/beyonce-adidas-first-look-01.jpg"
            rating={5}
          />
        </div>
      </div>
    </div>
  );
}

export default Home;
